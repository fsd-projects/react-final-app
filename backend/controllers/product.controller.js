const Product = require('../models/product');

const initialValues = [
  { name: "Basketball Shirt" , image: "https://images.footballfanatics.com/chicago-bulls/michael-jordan-white-chicago-bulls-autographed-nike-authentic-jersey-upper-deck_pi4384000_ff_4384900-099f2f5a6e30517b3cdc_full.jpg?_hv=2&w=340", price: 123, description: "Beautiful basketball shirt", category: 1},
  { name: "Basketball Pants" , image: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTExYTExEWFhYWFhYWFhYWFhYWFhYWGRYZFxYWFhYZHioiGRsnHBcWIzMjJystMDAwGCE5OzYvOiovMC0BCwsLDw4PGRERHDEnHycvLy8tLzIxLy8vMS8vLy8vLy8vMC8vLy8tLy8vLS8vLzEvMS8vLy8vLy8vLy8vLy8vL//AABEIAOEA4QMBIgACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAAAgEDBAYHBQj/xAA/EAABAwIDBAgBCgUEAwAAAAABAAIDBBEFEiEGMUFhBxMiMlFxgZGxFCNCUmJygpKh8DNDU6LBY8LR8TRzk//EABoBAQEAAwEBAAAAAAAAAAAAAAABAgMEBQb/xAA1EQACAQIDBQUGBQUAAAAAAAAAAQIDEQQhMRJBUWFxgZGhsfAFEyIy4fEjM0LB0RQkUpKi/9oADAMBAAIRAxEAPwDuKIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIqEqqAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiK3I7ggLUhurQaRucR8FcuiyIGzkbwrjKgFWyFbLADp+n/CAyxIPFVzDxWK137/fovCxvE5Y5AyMA9guILbk2PDUeSlirM2i6pmHiFo4x6pO5jd/FgB87GS9tR7r2sCq5ZGkysLTcW7BboQDuPmQoVqx7pkHioGpb4qw7XcoZOatiF81Q4AlQNUeACiB+/35lUcFbIGXE4nerix4HLIWICIiAIiIAiIgCIiAIiIAiIgCIiAoSrBClIbqCqIBayFDqq2VBGyi7ep7lB5tbmf8ICDljV1FHKA2VgcAdL6EXFjZw1GhPusws4KzY++qAs0lFHGAGMAyjTe4jyJ1WUEAP6qrR8P2UBIKipn+Nv37KjNW35n4oCpH6C6rl4cr/wCFQntjm34HX4qr2HhvG7n4hARjdY2Ky2OurDSHBSZcaIDIRUBVViUIiIAiIgCIiAIiIAiIgCi8qStyFAW1ABTUWHUqohLcqEoQlkQKAXUJT2mfi+CuhWpB2mnwuqC43erUbbW8rK6CrcaAqN55fG3/AEqRjtEeDWj4qUQ3+Z/4HwUYu848x8EBab3ncn392rJaLaLF3SnmAVlOHFAWph3T4E+xCutN1R25UYUAcLO89fXirm8KMzdLjgqMegL0ZVxWGlX1iUIiIAiIgCIiAIiIAiIgCsOKuvKxyUBIqMXFScqNCqIVuhVCFAuVBcuouGqo1TQEbqEPHzU3qMQ/VATZ/lQj4+amzj5lRYgLFTpI0+OiyhuWNX/RPgVkMKMBQU3BRsgJxlQkYqxpKdUAh3rIaVjsV1p1UYLqIihQiIgCIiAIiIAiLTukvGjBSujY60kwdG0je1tu04c9QBzdyUbSV2bKVKVWahBZsz4sbd8tlpZGgDq2TQOH02dyQH7Qf+hC9dpXM5MX6ykosTv26aRrKi2/q3HqJ7j8r10th8Fssai4VRqqqBYsFVQtVSiIEHBMyqVQNVBQlVboFRxQFASZuRqBEBbrW3Z5KUTtB5KUgu0q1D3fJAZBUXBVBQqAtg6qs28KTmq27eFQXMuig6oawOc9wa1oLnE6AAC5J9FO+i03bF5qJ4MPaTllvNU2Nj8njI7HIPfZvoUBulJUslY2SN7XscLtc0hzSPEEb1fWn7ObT0r6qajhjyOa52rQBFI9oAky23OB0OmuW63BYlCIiAIiIAiIgC4Tt9jPymrdY3jjORnhZp7R9Tc+Vl1va7EeopnuBs5/zbDxzP0uPIXd+FciZshUyDrIY+sZmLO81pBFuDiNNd4Wuom8kev7LdOm5VajtuT3c/p2mZ0bvbL8soH92eMyN5Ejq5PXVp9F0DYGvdLRxdZ/FivBKDvEkRyG/M2B9VyOgq/kddFJcWilMcpBu3K75uU34gEk+i6jg5+TYlUwbmVUbaqPw6xto5wOZ7Dlti/hOLGwUazktJWkuklfwd0bXmKmCqGQKDplTkLl0VnOUBclgX1FxVsBykGeKAFAFVEBIKhKKlkBJqtM8Fdarbt6AuMKrdRapFAUBUTqhRAVduXNIMYDGYhip+k4wU1+McR6uO3J0riVtm2+JOp6KZ7e+5ojiA3mSQ9Wy3q6/oud9IgFPTUeHtPcb1kluIYMovyc8vP4Vb2TZnTpupNQjq3Y1vZrEHU0sUtyS14c7xdc3dfzBcPVfRkMgc0OabhwBB8QRcFfMsRXc+jfEuuo2Am7orxnyABb/aQPRc8Hm+Z7ntbDpU4TjpH4ezd65m2IiLaeCEREARFh4pWNhifK/dG0uPOw3Dmd3qhUm3ZHPukjFgZmxZrNibmNuMjxut4htvzFc4mxyYFwjlfGCTcNc9uYHgbHVVxTEXzPe9297i51vEm/tqvOygau3eHitO1mfTYfAwpU/wAZLLu67lr4JImW3a5viD8V1BleZIcIrb3c2WOGU8pmOhkvyztaV4WA7IMbF8rxJ3UU7bOELrhz/q9bbXXgwanluW4U1BJXmJz4TT0ULmvhhtklmLO494H8KMcGjU+y3U47KzPJ9pYqnXqLY3ZX4/Q3INVQwLBxarlYGiJjHOJu7rH5GtjHfeTyuBbmmB4l8oi6zIWdt7bEg3yOLb+RIKyPOs7N+vWR6FkspKl0IFQlULlS6AIqIPFASVQrVNJmF+ZVxAFGRXFF40QFGqRVvNZUMqAkVJpVg3KuRhAartV8/W0VMNWsc+rkHKIZYv73fouY7WYgJq+Z7j2GSNguBezI9HkDj2s5sujSTiPGHmbsddSxxUznd17mvc+VrTwddzeydTZcpx7B56SUwzi5cXPZKO5Nc3LgeDtdW7x7Eyfynd7OjTlXtN2yaXVq3k3bnY9yo2bpo4jM2vil1GRsZbmLSdSRmJBF9RbRbh0bVbWuyAACRp08XM1Hn2c/suQMkcw3BcNeJW6bF4hks4d5jw+3iBq4DkRmHqtV1e6PRxdCtsONSW1ffp0Vrb/vc7oihG8OAINwQCD4g7lNbDwQiIgC570x4kI6WOPNbrZNw+k1gvYn7xYfRdCXPdt9tDTVHUGlZPF1bTIC6zg5xdpYgtIyhpsfFR2tmb8M5qqpU47TWdunQ48H33W9T/uW37IuoaeMVUxdUVVwI4WtJDHE2aGaWJva7zoOHPJMeCVpsM1FMeB+aufI3iPpYrBxXo+rYRmhLKhnjHZklvuE2PoSig45rM9OrjKOJXu621C3d25X9ao6BhmBSzSNqsQLXSN7UNM3WGmvuP8AqS+Lju4cLbWHlcEwzairpHZBI5hbvhmDreWR+o9LLfMI6S4X2bURmJ31hdzPbvD2PmqpreclT2dUS2qTU48Y/wAfc9HaeqnZO9zGFzOodG1hjc5j3kZw7ONO8A0tNri9uC2iij6uNrNBlAvbQZt7iBw1usagrYpm54pWvHi1wPofA+azBEfFbG726HDmlsvjcmZEzII1INWJiUCqllRAFMRh1wd1tVEK/TjRGUx2QhnZG7mpuUqgag+igEISaVWytE2U2PQEcqqGBXFBASCplUCCol5QGLjmDQ1URhmZmadQRo5jhuex30XDxWkYgzLbD8VHWRSG1PV7szvoskd/LnHA7ne9+hNkXi7U1lH1Loqx0eR4sY3aucPstHavzG7RL2KouT2UrvhqcS2p2emoZMkt3McfmpgNH/Zf9V9uHG1wsbD6hzO4bE+Hh6LY6nagNilo8vyumOkTpwRK1ttziw3dlPdd2Tp7adC8NswON9wGpJ9GrTKz+U+nwk60aVsUklucms+t/wB7M+hOj7Euvo2XNzGTEfwgFv8AaW+y2dc06H4pWibrIpWNcIywva5rXHt5subedQulrON7K54GL2Pfz927xvdW55+AREVOcLW8b2NpKol74y2R298bi1x0AuRq0mwG8HctkRLGcKk6b2oNp8jkuNdFsouYJWyt+pJ2Heh1a4+eVam2Ovw1wDHS0+ujXDPA7kAbtPm03X0MrM8LXtLXtDmnQtcAQRzB3rHZtpkdqx7mrV4qa7n3r7nG2bdxTNEeJULJG/1Imh4HPI7tM82uKN2NoqsF+G1gHEwvu4DkQbSM9QVuuM9HNJNd0V4HH6mrL82H/BC0HG+jmrhPWNj60t1D4CRIOeTRwP3bq7T/AFIyhToye1h6jhLhLLukvJ5veeTV4ViFA/rDHIzL/NicXMt5t7o5OC2DA+lGVthNGJm/WaMsnmR3SfQLzcO2xr6Y5XP68N3tnBDxy6wWN/vArNlxnCaw2rKZ1JKf5rdGk+PWsFj+NqiUX8rNtapWjli6Skv8tH/tHyOhYPtpR1Fg2UMcfoy9g38AdzvQlbBdcXxHo+my9bRzx1UZ1ALmh5H2XjsP/ReZQ7SV1C8RuMkdv5UzSWkDTs5t45ghZXa1Rz/02Hrfk1LPhL9msn5nekK0XA+kmCSzahpid9axdEfXvN9rc1ulNUMkaHxva9p3OaQ4HyIVUk9DlrYerRdqkWvLv08S8sqNtgAsWDV1vBZqM1FuZtx+qxmOWavPnGV3I6hEC64Ky42V0OWPVzsY3M97WAcXENHuVSF5swVesC0rGdvaSHRrjK7/AEx2fzmwI8rrRsb6R6h9wwtgbyvn/Nb4ALFyidtPAV5raa2Y8ZZLxz8DstbicMTc0srIx4ucG/HetQxjpMgjuIY3Snx7jP1GY+y5lRYPXVjs7IpH33ySEsbb78mrh5XWyxbDU1MwS4nWtYDuijOUHlmtnf5NAT4npkbfd4OjnOTm+CyXfr3MwcW6Q6uY5Gv6vNoGQtOc8ri7r+yhh+xuIVRzOi6prtS+occx55BdxPnZeo3bWlpgWYbQDw62T5sHnxkf6kLwK/HsQq3ZJJ367ooAY2nl2e271JWL2Fq7nRTlipxtQgoR46Zc29ew9ufZjDKTWvrjK8b4mnLc/wDqju/9VWPbynp+zQYa1v23hsV+ZAu93qQVhYJ0b1cliIWwNP0pSWu/LYuvyIC3bCOiunZYzyPmI4D5tnrl7R9wrtPcjRKlh4u9eq5S4Rz/AOn5ZHlbI7cVc1XGyofE2N5LMkbD3nA5O04k96y6yvNw/BKeD+DBGzmGjMfN28+69JFfecdeVKUvwotLm7358giIqaQiIgCIiAIiIDzMVwOnqRaeFj/AkWePuvHab6Fc9x/ovcLupJA4f0pSAfJrxofJw9V1VFHFPU6KGKrUfkllw3d38HzaYKmhls0zUsv1R3X88puyQc9VslJt91jepxKlZNGd8kYBtzdE7jzafILsWIYfFMwxzRskafovaHDz1481oOPdFzDd1JJkP9OQlzPJr+831zItqOh1e9wmIyqx2JcY6dq9dTXZdiaepYZsLqWkcYXuLgDvtc9uM8nArWoMUq8Omy9uB/0mO1Y8eP1XjmPdSxLCaiilDnNkp5B3ZG6ZvJw7Lxy9wt/2EqpMSa5ldTRSshLSJrCz33vbIe66wBJBtY7tVLxlyZtfv8LDVTpPtX07Mrm57IVsk9MyWWLqnv1y+LeDwDqAd9ivdRFkeVJpybSsuHALxtqamSKmfLDGJHsGYNN91+0bDU2FzbjZeyiEi0mm1fkcBr+kesf2esbHwtG2xN931ne1lCk2dxKsOfqngHdJUSFnsDd9vILZ9ocVpsJndFTYa3rC1rxIcrGWcTaz9XEAgi2m5aviO1lfU6OqOrDtBHTgsv4DPcvJ9Vi1FfM7nsUZ4mov7amoLjZXt1evYj13bB01OA/EMQa37DCI78rm7n+gChFtLhVL/wCDQmZ4vaR4LR/9JbvHo1YWE9H9XOc/UFmbe+clrjz1u4+y3jCui6FtjUSvlPEN+bZ6nUn3CKT/AEo01KVJO+IrbT4Rzfe8l0NFr9ssQqTkZIIQ7QNp2lzzy6w3N/ugK9g/R1WTu6x7Cwm15KhxMh/Cbuv52XZ8MweCnblhhZGOOVoBP3nb3eq9BLN6s1/1lOn+RTS5v4n1W5Gg4X0YUzLGd75j4D5tns05j+Zbhh2GQwC0MTIx9hobfzI3+qzkVSS0OariKtZ3qSb9cNPAIiKmkIiIAiIgCIiAIiIAiIgCIiAIiIDHq6ZkrSyRjXtdoWuAc0+YKsYVhkVOwRwxhjAScovvcbk3Ov7A4LPRC3drXyCIiECIiA1/aPZOnrTG6cOvHe2V2XMDa7XG17XF9LcVmYXgdPTC0MLGaWuBdx83ntH1K9REsZurNxUHJ2W6+QREQwCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiID/2Q==", price: 123, description: "Beautiful basketball pants", category: 1},
  { name: "Football Shirt" , image: "https://img.fruugo.com/product/9/75/348972759_max.jpg", price: 123, description: "bla bla", category: 2},
  { name: "Football Pants" , image: "https://banner2.cleanpng.com/20180621/ztf/kisspng-fc-barcelona-fcbotiga-bm-granollers-jersey-pants-barcelona-fc-5b2c095da8e775.5259108215296126376918.jpg", price: 123, description: "allalaa", category: 2},
  { name: "Tennis Shoes" , image: "https://static.nike.com/a/images/t_default/3fd0138b-8fd6-4923-a3fb-3aae073623ce/nikecourt-react-vapor-nxt-mens-hard-court-tennis-shoes-FxT1HX.png", price: 250, description: "asdasdasd", category: 3},
  { name: "Running Shoes" , image: "https://media.terminalx.com/pub/media/catalog/product/cache/f112238e8de94b6d480bd02e7a9501b8/z/2/z205760011-11635945711.jpg", price: 404, description: ":) :) :)", category: 4}
];

const initProducts = () => {
  Product.create(initialValues, (err, res) => {
    if (err) console.log(err);
    console.log(`Inserted ${res.length} documents`);
  });
}

const getProductById = (req, res) => {
  Product.findOne({id: req.query.id}).populate('category').then((response) => {
    res.send(response);
  }).catch((error) => {
    console.log(`there was a problem...${e.message}`);
  });
}

const getAllProducts = (req, res) => {
  Product.find().populate('category').then((response) => {
    res.send(response);
  }).catch((error) => {
    console.log(`there was a problem...${e.message}`);
  });
}

const createProduct = (req, res) => {
  Product.create(
    {
      ...req.body.params
    }
  ).then((response) => {
    res.send(response);
  }).catch((error) => {
    console.log(`there was a problem...${e.message}`);
  });
}

module.exports = {
  getProductById,
  getAllProducts,
  createProduct,
  initProducts
}