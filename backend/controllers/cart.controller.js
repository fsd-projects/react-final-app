const Cart = require('../models/cart');
const User = require("../models/user");
const Product = require("../models/product");

const initialValues = [
    {user: 51, products: []},
    {user: 52, products: []},
    {user: 54, products: []},
    {user: 53, products: []},
    {user: 81, products: []},
    {user: 82, products: []},
    {user: 83, products: []},
];

const initCarts = () => {
    Cart.create(initialValues, (err, res) => {
        if (err) console.log(err);
        console.log(`Inserted ${res.length} documents`);
    });
}

const getCartByUserId = (req, res) => {
    Cart.findOne({user: req.query.id}).deepPopulate('products.product').then((response) => {
        res.send(response);
    }).catch((e) => {
        console.log(`there was a problem...${e.message}`);
    });
}

async function removeProductFromCart(req, res) {
    try {
        const currentCart = await Cart.findOne({
            user: req.body.userId
        }).deepPopulate('products.product');
        const currentProduct = currentCart.products.filter(product => product._doc.product._doc._id === req.body.productId);

        if (currentProduct.length === 0) {
            console.log('the product is not in the cart')
        } else {
            currentProduct[0]._doc.quantity -= 1;
            if (currentProduct[0]._doc.quantity === 0) {
                currentCart.products = currentCart.products.filter(p => p.product._id !== req.body.productId)
            } else {
                currentCart.products.map(p => p.product._id !== req.body.productId ? p : currentProduct[0])
            }
        }

        currentCart.markModified('products');
        res.send(await currentCart.save())
    }
    catch(err) {
        console.log(err);
    }
}

async function addProductToCart(req, res) {
    try {
        const currentCart = await Cart.findOne({
            user: req.body.userId
        }).deepPopulate('products.product');
        const currentProduct = currentCart.products.filter(product => product._doc.product._doc._id === req.body.productId);

        if (currentProduct.length === 0) {
            const product = await Product.findById(req.body.productId);
            currentCart.products.push({product: product, quantity: 1});
        } else {
            currentProduct[0]._doc.quantity += 1;
            currentCart.products.map(p => p.product._id !== req.body.productId ? p : currentProduct[0])
        }

        currentCart.markModified('products');
        res.send(await currentCart.save())
    }
    catch(err) {
        console.log(err);
    }
}

async function emptyCart(req, res) {
    try {
        const currentCart = await Cart.findOne({
            user: req.body.userId
        }).deepPopulate('products.product');
        currentCart.products = [];

        currentCart.markModified('products');
        res.send(await currentCart.save())
    }
    catch(err) {
        console.log(err);
    }
}

module.exports = {
    getCartByUserId,
    initCarts,
    addProductToCart,
    removeProductFromCart,
    emptyCart
}