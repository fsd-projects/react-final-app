const Category = require('../models/category');

const initialValues = [
  { _id: 1, name: "Basketball" },
  { _id: 2, name: "Football" },
  { _id: 3, name: "Swimming" },
  { _id: 4, name: "Running" },
  { _id: 5, name: "Casual" },
  { _id: 6, name: "Yoga" }
];

const initCategories = () => {
  Category.create(initialValues, (err, res) => {
    if (err) console.log(err);
    console.log(`Inserted ${res.length} documents`);
  });
}

const getCategoryById = (req, res) => {
  Category.findOne({id: req.query.id}).then((response) => {
    res.send(response);
  }).catch((error) => {
    console.log(`there was a problem...${e.message}`);
  });
}

const getAllCategories = (req, res) => {
  Category.find().then((response) => {
    res.send(response);
  }).catch((error) => {
    console.log(`there was a problem...${e.message}`);
  });
}

const createCategory = (req, res) => {
  Category.create(
    {
      ...req.body.params
    }
  ).then((response) => {
    res.send(response);
  }).catch((error) => {
    console.log(`there was a problem...${e.message}`);
  });
}

module.exports = {
  initCategories,
  getCategoryById,
  getAllCategories,
  createCategory
}