const User = require('../models/user');
const Cart = require("../models/cart");

const initialValues = [
  { uid: "69K4JhQQzVa9zlZdvY2U21oGlzs2", name: "Ofri", mail: "Olahad24@gmail.com", phoneNumber: "0543329405", address: "Hazorim 54, Shdema",  role: "Customer"},
  { uid: "tnUubwbImeYuITdcSJw0qZGmJc43", name: "Mika", mail: "mika.y.kost@gmail.com", phoneNumber: "0525252525", address: "Tel Aviv",  role: "Customer"},
  { uid: "kQyODqlHwNbvZTLrYPOiflCclOv2", name: "Eden", mail: "shaul.eden@gmail.com", phoneNumber: "0545454545", address: "Rishon Leziyon",  role: "Customer"},
  { uid: "LV4Z26YHwaUyOGn55VV3W9h6Xdh1", name: "admin", mail: "admin@admin.com", phoneNumber: "0525381648", address: "Somewhere in Israel",  role: "Admin"}
];

const initUsers = () => {
  User.create(initialValues, (err, res) => {
    if (err) console.log(err);
    console.log(`Inserted ${res.length} documents`);
  });
}

const getUserById = (req, res) => {
  User.findOne({id: req.query.id}).then((response) => {
    res.send(response);
  }).catch((e) => {
    console.log(`there was a problem...${e.message}`);
  });
}

const getUserByUid = (req, res) => {
  User.findOne({uid: req.query.uid}).then((response) => {
    res.send(response);
  }).catch((e) => {
    console.log(`there was a problem...${e.message}`);
  });
}

const getAllUsers = (req, res) => {
  User.find().then((response) => {
    res.send(response);
  }).catch((e) => {
    console.log(`there was a problem...${e.message}`);
  });
}

const createUser = (req, res) => {
  User.create(
    {
      ...req.body
    }
  ).then((response) => {
    res.send(response);
    Cart.create(
      {
        user: response._id,
        products: []
      }
  ).then((response) => {
    console.log(response);
  }).catch((e) => {
    console.log(`there was a problem...${e.message}`);
  });
  }).catch((e) => {
    console.log(`there was a problem...${e.message}`);
  });
}

module.exports = {
  getUserById,
  getAllUsers,
  createUser,
  initUsers,
  getUserByUid
}