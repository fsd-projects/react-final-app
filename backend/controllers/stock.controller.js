const Stock = require('../models/stock');
const updateProductQuantity = require("../BL/stock-handler")

const initialValues = [
  { product: 1, quantity: 101},
  { product: 2, quantity: 102},
  { product: 3, quantity: 103},
  { product: 4, quantity: 104},
  { product: 5, quantity: 105},
  { product: 6, quantity: 106}
];

const initStocks = () => {
  Stock.create(initialValues, (err, res) => {
    if (err) console.log(err);
    console.log(`Inserted ${res.length} documents`);
  });
}

const getStockByProductId = (req, res) => {
  Stock.findOne({product: req.query.id}).then((response) => {
    res.send(response);
  }).catch((e) => {
    console.log(`there was a problem...${e.message}`);
  });
}

const getAllStocks = (req, res) => {
  Stock.find().populate('product').then((response) => {
    res.send(response);
  }).catch((e) => {
    console.log(`there was a problem...${e.message}`);
  });
}

const createStock = (req, res) => {
  Stock.create(
    {
      ...req.body.params
    }
  ).then((response) => {
    res.send(response);
  }).catch((e) => {
    console.log(`there was a problem...${e.message}`);
  });
}

const updateStockByProductId = (req, res) => {
  updateProductQuantity(req.body.product, req.body.quantity, res);
}

module.exports = {
  getStockByProductId,
  getAllStocks,
  createStock,
  updateStockByProductId,
  initStocks
}