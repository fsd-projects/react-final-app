const bodyParser = require('body-parser');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const createError = require('http-errors');
const express = require('express');
const {default: mongoose} = require('mongoose');
const logger = require('morgan');
const methodOverride = require('method-override');
const mongoUrl =
    'mongodb+srv://ReactFinalProject:ReactOEM123@reactcluster.jbf3fod.mongodb.net/SportsStore';
const path = require('path');
const VerifyToken = require("./middleware/VerifyToken");

defineModels();

const {getUserByUid, getUserById, getAllUsers, initUsers, createUser} = require('./controllers/user.controller');
const {
    createCategory,
    initCategories,
    getCategoryById,
    getAllCategories
} = require('./controllers/category.controller');
const {
    getOrdersByUser,
    initOrders,
    getAllOrders,
    getLastWeekOrders,
    getOrdersByUserId,
    createOrder
} = require('./controllers/order.controller');
const {
    initCarts,
    emptyCart,
    removeProductFromCart,
    getCartByUserId,
    addProductToCart
} = require('./controllers/cart.controller');
const {
    initStocks,
    getStockByProductId,
    getAllStocks,
    createStock,
    updateStockByProductId
} = require('./controllers/stock.controller');
const {initProducts, getAllProducts, getProductById, createProduct} = require('./controllers/product.controller');
const {scrape} = require('./scraping/scraper')
const {startSocketIOConnection} = require('./realtime-service/socket-io-service')

const app = express();

initializeApp();

mongoose.set('strictQuery', false);

mongoose
    .connect(mongoUrl, {useNewUrlParser: true})
    .then(() => {
        console.log('connection started');
    })
    .catch((err) => {
        console.log('connection error:' + err);
    });

app.post('/emptyCart', async (req, res) => {
    await emptyCart(req, res)
});

app.post('/removeProductFromCart', async (req, res) => {
    await removeProductFromCart(req, res);
});

app.post('/addProductToCart', async (req, res) => {
    await addProductToCart(req, res);
});

app.get('/getCartByUserId', async (req, res) => {
    getCartByUserId(req, res);
});

app.post('/initCategories', async () => {
    initCategories();
});

app.post('/initOrders', async () => {
    initOrders();
});

app.get('/getOrdersByUser', async (req, res) => {
    getOrdersByUser(req, res);
});

app.post('/initProducts', async () => {
    initProducts();
});

app.post('/initStocks', async () => {
    initStocks();
});

app.post('/initUsers', async () => {
    initUsers();
});

app.get('/getCategoryById', async (req, res) => {
    getCategoryById(req, res);
    console.log(res);
});

app.get('/getAllCategories', async (req, res) => {
    getAllCategories(req, res);
    console.log(res);
});

app.post('/createCategory', async (req, res) => {
    createCategory(req, res);
    console.log(res);
});

app.get('/getOrdersByUserId', async (req, res) => {
    getOrdersByUserId(req, res);
    console.log(res);
});

app.get('/getAllOrders', async (req, res) => {
    getAllOrders(req, res);
    console.log(res);
});

app.get('/getLastWeekOrders', async (req, res) => {
    getLastWeekOrders(req, res);
    console.log(res);
});

app.post('/createOrder', async (req, res) => {
    createOrder(req, res);
    console.log(res);
});

app.get('/getProductById', async (req, res) => {
    getProductById(req, res);
    console.log(res);
});

app.get('/getAllProducts', async (req, res) => {
    getAllProducts(req, res);
    console.log(res);
});

app.post('/createProducts', async (req, res) => {
    createProduct(req, res);
    console.log(res);
});

app.get('/getStockByProductId', async (req, res) => {
    getStockByProductId(req, res);
    console.log(res);
});

app.get('/getAllStocks', async (req, res) => {
    getAllStocks(req, res);
    console.log(res);
});

app.post('/createStock', async (req, res) => {
    createStock(req, res);
    console.log(res);
});

app.put('/updateStockByProductId', async (req, res) => {
    updateStockByProductId(req, res);
    console.log(res);
});

app.get('/getUserById', async (req, res) => {
    getUserById(req, res);
    console.log(res);
});

app.get('/user', async (req, res) => {
    getUserByUid(req, res);
    console.log(res);
});

app.get('/getAllUsers', async (req, res) => {
    getAllUsers(req, res);
    console.log(res);
});

app.post('/createUser', async (req, res) => {
    createUser(req, res);
    console.log(res);
});

app.use(function (req, res, next) {
    next(createError(404));
});

app.use(function (err, req, res, next) {
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;

function initializeApp() {
    app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'jade');
    app.use(logger('dev'));
    app.use(express.json());
    app.use(cookieParser());
    app.use(express.static(path.join(__dirname, 'public')));
    app.use(cors());
    app.use(bodyParser.json());
    app.use(methodOverride('_method'));
    app.use(express.urlencoded({extended: true}));
    app.use(VerifyToken);
    startSocketIOConnection()
    // scrape();
    // initCarts();
}

function defineModels() {
    const Category = require('./models/category');
    const Product = require('./models/product');
    const Order = require('./models/order');
    const Stock = require('./models/stock');
    const User = require('./models/user');
}

app.listen(3001, () => {
    console.log('listening on port 3001!');
});
