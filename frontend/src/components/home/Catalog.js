import {Button, Form, Row} from 'react-bootstrap';
import Product from '../product/Product';
import styled from 'styled-components';
import useGet from '../../utils/requests/useGet';
import AsyncDataLoaderWrapper from '../appearence/AsyncDataLoaderWrapper';
import {useEffect, useState} from "react";

export default function Catalog() {
    const {data, loading} = useGet('getAllStocks');
    const [brandSearch, setBrandSearch] = useState("");
    const [descriptionSearch, setDescriptionSearch] = useState("");
    const [maxPriceSearch, setMaxPriceSearch] = useState("");
    const [filteredData, setFilteredData] = useState([]);

    useEffect(() => {
        setFilteredData(data);
    }, [data]);

    const handleFormSubmit = (e) => {
        e.preventDefault();
        setFilteredData(data.map(item => item).filter(product =>
            (product.product.name.toLowerCase().includes(brandSearch ? brandSearch.toLowerCase() : "")) &&
            (product.product.description.toLowerCase().includes(descriptionSearch ? descriptionSearch.toLowerCase() : "")) &&
            (maxPriceSearch && maxPriceSearch !== 0 ? product.product.price <= maxPriceSearch : true)));
    };

    return (
        <AsyncDataLoaderWrapper loading={loading} text="Loading products...">
            <CatalogContainer>
                <SearchContainer>
                    <SearchForm onSubmit={handleFormSubmit}>
                        <SearchInput className="form-control"
                                     type="text"
                                     placeholder="Brand..."
                                     value={brandSearch}
                                     onChange={(e) => setBrandSearch(e.target.value)}/>
                        <SearchInput className="form-control"
                                     type="text"
                                     placeholder="Description..."
                                     value={descriptionSearch}
                                     onChange={(e) => setDescriptionSearch(e.target.value)}/>
                        <SearchInput className="form-control"
                                     type="number"
                                     id=""
                                     placeholder="Max Price"
                                     value={maxPriceSearch}
                                     onChange={(e) => setMaxPriceSearch(e.target.value)}/>
                        <SearchButton type='submit' variant="info">Search</SearchButton>
                    </SearchForm>
                </SearchContainer>
                <Row xs={1} md={4} className="g-5">
                    {filteredData && filteredData.map((stock) => (
                        <Product key={stock.product._id} product={stock.product} quantity={stock.quantity}/>
                    ))}
                </Row>
            </CatalogContainer>
        </AsyncDataLoaderWrapper>
    );
}

export const SearchInput = styled.input`
  width: 20rem;
  margin: 1em 2em 1em 2em;
`;

export const SearchForm = styled(Form)`
  display: flex;
`;

export const SearchButton = styled(Button)`
  width: 20rem;
  margin: 1em 0 1em auto;
`;

export const SearchContainer = styled(Row)`
  margin: 1em 0 1em 0;
`;

const CatalogContainer = styled.div`
  margin: 0 13rem 0 13rem;
`;
